<?php
/**
 * Plugin Name:       GwadaSlide
 * Description:       Manage a slider
 * Version:           0.1
 * Author:            Andrew MONDOR X Bilal BELATOUI X Eyram DE SOUZA X Nasfahdine MMADI
 */


 add_action('init', 'slider_init');
 
 /**
  * Initialiser les fonctionnalités liées au carrousel
  */
 function slider_init(){
     /**
      * Modifier les valeurs du menu
      */
     $labels = array (
         'name' => 'Slides',
         'singular_name' => 'Slide',
         'add_new' => 'Ajouter un Slide',
         'add_new_item' => 'Ajouter un nouveau Slide',
         'edit_item' => 'Editer un Slide',
         'new_item' => 'Nouvelle Slide',
         'view_item' => 'Voir l\'Slide',
         'search_item' => 'Rechercher un Slide',
         'not_found' => 'Pas de Slide',
         'not_found_in_trash' => 'Pas de slide dans la corbeille',
         'parent_item_colon' => '',
         'menu_name' => 'Slides'


     );
     
    register_post_type('slide', array(
        'public' => true,
        'publicity_queryable' => false,
        'labels' => $labels,
        'menu_position' => 9,
        'capability_type'=> 'post',
        'supports'=> array('title', 'thumbnail'), 
    ));

    add_image_size('slider', 1000,300,true);
 }

 /**
  * Permet d'afficher le slider
  */
function slider_show($limit = 10){
    
    $slides = new WP_query("post_type=slide&posts_per_page=$limit");
    echo '<div id="slider">';
    while($slides->have_posts()){
        $slides->the_post();
        global $post;
        the_post_thumbnail('sliders') ; 
    }
    echo '</div>';
} 